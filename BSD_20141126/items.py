# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class BSD_20141126Item(Item):
    title = Field()
    content = Field()
    cost = Field()
    area = Field()
    location = Field()
    date = Field()
