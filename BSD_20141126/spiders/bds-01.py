# -*- coding: utf-8 -*-
from scrapy.selector import Selector
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from BSD_20141126.items import BSD_20141126Item
from BSD_20141126.utils import extractor


class BatdongsanSpider(CrawlSpider):
    name = 'batdongsan'
    allowed_domains = ['batdongsan.com.vn']
    start_urls = [
        'http://batdongsan.com.vn/nha-dat-ban-hai-ba-trung/p1']

    rules = (
        Rule(LxmlLinkExtractor(allow=(r'.*/nha-dat-ban-hai-ba-trung//p\d+', r'.*/nha-dat-ban-hai-ba-trung')), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        sel = Selector(response)

        for node in sel.xpath("//div[contains(@class, 'vip0')]"):
            i = BSD_20141126Item()
            i["title"] = extractor(node, ".//div[@class='p-title']/a/text()").strip()
            i["content"] = extractor(node, "string(.//div[@class='p-content']/div)").strip()
            i["cost"] = extractor(node, ".//span[@class='product-price']/text()").strip()
            i["area"] = extractor(node, ".//span[@class='product-area']/text()").strip()
            i["location"] = extractor(node, ".//span[@class='product-city-dist']/text()").strip()
            i["date"] = extractor(node, ".//div[@class='floatright mar-right-10']/text()").strip()
            yield i


