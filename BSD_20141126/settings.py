# Scrapy settings for fl2117529 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'BSD_20141126'

SPIDER_MODULES = ['BSD_20141126.spiders']
NEWSPIDER_MODULE = 'BSD_20141126.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"

DOWNLOAD_DELAY = 1
DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.ajaxcrawl.AjaxCrawlMiddleware': 90,
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
}

AUTOTHROTTLE_ENABLED = True
CONCURRENT_REQUESTS = 100
AJAXCRAWL_ENABLED = True
LOG_LEVEL = 'DEBUG'
