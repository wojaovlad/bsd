__author__ = 'Vazhko'


def extractor(xpathselector, selector, idx=0):
    """
    Helper function that extract info from xpathselector object
    using the selector constrains.
    """
    val = xpathselector.xpath(selector).extract()
    return val[idx] if val else None
