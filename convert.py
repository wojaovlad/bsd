# -*- coding: utf-8 -*-
import csv
import datetime
import time
now_time = datetime.datetime.now()
ifile = open('results.csv', "rb")
rfile = open('bds-' + now_time.strftime("%Y.%m.%d-%H-%M-%S") + '.txt', 'w')
reader = csv.reader(ifile)
records = (sum(1 for l in open('results.csv', 'r')))
rfile.write("Date of search: %s\n" % now_time.strftime("%d.%m.%Y") + "Start time: %s\n" % now_time.strftime("%H:%M:%S") + "URL: http://batdongsan.com.vn/nha-dat-ban-hai-ba-trung \n" + "Total found records: %s\n" % records + "____________________________________________________________\n\n")

def convertor():
	number = 1
	rownum = 0
	for row in reader:
	    if rownum == 0:
	        header = row
	    else:
	        colnum = 0
	        for col in row:
		    rfile.writelines('%8s: %s \n' % (header[colnum], col))
#	            print '%8s: %s \n' % (header[colnum], col)
	            colnum += 1

	    rfile.writelines('\nNumber: %s' % str(number) + ".\n\n")
	    number += 1
	    rownum += 1


	print "Please wait..."
	time.sleep(3)
	ifile.close()
	rfile.close()

if __name__ == '__main__':
	convertor()
